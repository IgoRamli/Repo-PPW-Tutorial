deploy: clean create_virtualenv install_requirements prep_deploy run

prep_deploy: collect_static migrate

create_virtualenv:
	virtualenv venv
	. venv/bin/activate

install_requirements: create_virtualenv
	pip3 install -r requirements.txt
	
collect_static:
	echo 'yes' | python3 manage.py collectstatic
	
migrate:
	python3 manage.py makemigrations
	python3 manage.py migrate
	
clean:
	find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
	
run:
	python3 manage.py runserver
